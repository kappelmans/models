package model

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

const (
	Sexe_Male = iota
	Sexe_Female
)

const (
	LegalPersonType_NV = iota
	LegalPersonType_BVBA
	LegalPersonType_VZW
)

const (
	LanguageNL = iota
	LanguageFR
	LanguageEN
)

type PrivatePerson struct {
	FirstName  string     `json:"firstname"`
	LastName   string     `json:"lastname"`
	Birthdate  *time.Time `json:birthdate,omitempty`
	BirthPlace string     `json:"birthplace"`
	Sexe       int        `json:"sexe"`
}

type LegalPerson struct {
	Name             string `json:"name"`
	EnterpriseNumber string `json:"enterprisenbr"`
	Type             int    `json:"type"`
}

type Party struct {
	ID            bson.ObjectId  `bson:"_id,omitempty" json:"_id,omitempty"`
	DataOwnerID   string         `bson:"dataowner,omitempty" json:"dataowner,omitempty"`
	OwnRef        *string        `bson:"ownref,omitempty" json:"ownref,omitempty"`
	PrivatePerson *PrivatePerson `bson:"privateperson,omitempty" json:"privateperson,omitempty"`
	LegalPerson   *LegalPerson   `bson:"legalperson,omitempty" json:"legalperson,omitempty"`
	Name          string         `bson:"-" json:"name"`
	Address       *Address       `json:"address"`
	Sex           int            `json:"sex"`
	Language      int            `json:"language"`
	Phone         string         `json:"phone"`
	Email         string         `json:"email"`
	CategoryList  []int          `json:"categorylist"`
}
