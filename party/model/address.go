package model

import "gopkg.in/mgo.v2/bson"

type Address struct {
	ID      bson.ObjectId `bson:"_id,omitempty" json:"_id,omitempty"`
	City    string        `json:"place"`
	Street  string        `json:"street"`
	ZipCode string        `json:"postalcode"`
}
