package requesthandler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	genericrequesthandler "gitlab.com/kappelmans/go-lang-backend/requesthandler"
	"gitlab.com/kappelmans/models/party/controller"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//TODO MAKE IT GENERIC!
func getCollection(db *mgo.Database, collectionName string) *mgo.Collection {
	return db.C(collectionName)
}

func GetPartyHandler(db *mgo.Database, c *gin.Context) {
	c.JSON(http.StatusOK, controller.GetParty(getCollection(db, "party"), c.Param("party-key")))
}

func GetPartyListHandler(db *mgo.Database, c *gin.Context) {
	datownerId := c.GetHeader("datownerId")
	c.JSON(http.StatusOK, controller.GetPartyList(getCollection(db, "party"), datownerId))
}

func DeletePartyHandler(db *mgo.Database, c *gin.Context) {
	partyKey := c.Param("party-key")
	getCollection(db, "party").RemoveId(bson.ObjectIdHex(partyKey))
	c.JSON(http.StatusOK, "Remove OK: ")
}

func GetPartyDetailHandler(db *mgo.Database, c *gin.Context) {
	partyKey := c.Param("party-key")
	c.JSON(http.StatusOK, controller.GetPartyDetail(getCollection(db, "party"), partyKey))
}

func CreatePartyHandler(db *mgo.Database, c *gin.Context) {
	//Retrieve body from http request
	b, err := c.GetRawData()
	datownerId := c.GetHeader("DatownerId")

	if err != nil {
		panic(err)
	}

	jsonString, _ := controller.CreateParty(getCollection(db, "party"), datownerId, b)

	c.JSON(http.StatusOK, jsonString)
}

func GetRoutes(basePath string) []genericrequesthandler.Route {

	return []genericrequesthandler.Route{
		genericrequesthandler.Route{
			Action:        genericrequesthandler.GET,
			Path:          basePath + "/list",
			ReqestHandler: GetPartyListHandler,
		},
		genericrequesthandler.Route{
			Action:        genericrequesthandler.GET,
			Path:          basePath + "/detail/:party-key",
			ReqestHandler: GetPartyDetailHandler,
		},
		genericrequesthandler.Route{
			Action:        genericrequesthandler.POST,
			Path:          basePath + "/create",
			ReqestHandler: CreatePartyHandler,
		},
		genericrequesthandler.Route{
			Action:        genericrequesthandler.DELETE,
			Path:          basePath + "/delete/:party-key",
			ReqestHandler: DeletePartyHandler,
		},
	}

}
