package controller

import (
	"encoding/json"
	"fmt"

	"gitlab.com/kappelmans/models/party/model"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func GetParty(col *mgo.Collection, key string) model.Party {
	results := []model.Party{}
	col.FindId(bson.ObjectIdHex(key)).All(&results)
	return results[0]
}

func GetPartyList(col *mgo.Collection, dataOwnerId string) []model.Party {
	results := []model.Party{}
	col.Find(bson.M{"dataowner": bson.M{"$eq": dataOwnerId}}).All(&results)
	fmt.Println("Search DataOwnerID:", dataOwnerId)
	for i, elem := range results {
		fmt.Println(elem.PrivatePerson)
		fmt.Println(elem.PrivatePerson != nil)
		if elem.PrivatePerson != nil {
			results[i].Name = fmt.Sprintf("%s %s", elem.PrivatePerson.FirstName, elem.PrivatePerson.LastName)
		} else if elem.LegalPerson != nil {
			results[i].Name = elem.LegalPerson.Name
		}
	}
	return results
}

func GetPartyDetail(col *mgo.Collection, partyKey string) model.Party {
	results := []model.Party{}
	col.FindId(bson.ObjectIdHex(partyKey)).All(&results)
	return results[0]
}

func CreateParty(col *mgo.Collection, datownerId string, jsonCreateBarray []byte) ([]byte, error) {

	//Save data into Job struct
	var _party model.Party

	_party.ID = bson.NewObjectId()

	err := json.Unmarshal(jsonCreateBarray, &_party)
	_party.DataOwnerID = datownerId

	fmt.Println("_party.DataOwnerID:", _party.DataOwnerID)

	if err != nil {
		fmt.Println("Err2", err.Error())
		//			http.Error(w, err.Error(), 500)
		return nil, err
	}

	//Insert job into MongoDB
	err = col.Insert(&_party)
	if err != nil {
		panic(err)
	}

	//Convert job struct into json
	jsonString, err := json.Marshal(_party)
	if err != nil {
		fmt.Println("Err1", err.Error())
		//		http.Error(w, err.Error(), 500)
		return nil, err
	}
	fmt.Println("OK!", string(jsonString))
	return jsonString, nil

}

func DeleteParty(col *mgo.Collection, key string) error {
	return col.RemoveId(bson.ObjectIdHex(key))
}
