package model

type ContactDetail struct {
	Type   int    `json:"type"`
	Detail string `json:"detail"`
}
