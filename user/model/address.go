package model

type Address struct {
	City string `json:"city"`

	Street      string `json:"street"`
	HouseNumber string `json:"housenumber"`
	ZipCode     string `json:"zipcode"`
}
