package model

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var col *mgo.Collection

type User struct {
	ID             bson.ObjectId   `bson:"_id,omitempty" json:"_id,omitempty"`
	FirstName      string          `json:"firstname"`
	LastName       string          `json:"lastname"`
	BirthDate      time.Time       `json:"birthdate"`
	Password       string          `json:"-"`
	Email          string          `json:"email"`
	Age            int             `json:"age"`
	ContactDetails []ContactDetail `json:"contactdetaillist"`
	Address        Address         `json:"address"`
}

func SetCollection(collection *mgo.Collection) {
	col = collection
}

func GetUsers(col *mgo.Collection) string {
	results := []User{}

	col.Find(bson.M{"firstname": bson.RegEx{"", ""}}).All(&results)

	jsonString, err := json.Marshal(results)
	if err != nil {
		panic(err)
	}

	return string(jsonString)
}

func UserGetHandler(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Access-Control-Allow-Origin", "*")

	fmt.Fprint(w, GetUsers(col))

}
