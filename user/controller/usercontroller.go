package controller

import (
	"encoding/json"
	"fmt"

	"gitlab.com/kappelmans/models/user/model"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func GetUserList(col *mgo.Collection) []model.User {
	results := []model.User{}

	col.Find(bson.M{"firstname": bson.RegEx{"", ""}}).All(&results)

	return results
}

func GetUsers(col *mgo.Collection) string {
	results := []model.User{}

	col.Find(nil).All(&results)

	jsonString, err := json.Marshal(results)
	if err != nil {
		panic(err)
	}

	return string(jsonString)
}

func CreateUser(col *mgo.Collection, jsonCreateBarray []byte) ([]byte, error) {

	//Save data into Job struct
	var _user model.User
	err := json.Unmarshal(jsonCreateBarray, &_user)
	if err != nil {
		fmt.Println("Err2", err.Error())
		//			http.Error(w, err.Error(), 500)
		return nil, err
	}

	//Insert job into MongoDB
	err = col.Insert(_user)
	if err != nil {
		panic(err)
	}

	//Convert job struct into json
	jsonString, err := json.Marshal(_user)
	if err != nil {
		fmt.Println("Err1", err.Error())
		//		http.Error(w, err.Error(), 500)
		return nil, err
	}
	fmt.Println("OK!")
	return jsonString, nil

}

func DeleteUser(col *mgo.Collection, key string) error {
	return col.RemoveId(bson.ObjectIdHex(key))
}
