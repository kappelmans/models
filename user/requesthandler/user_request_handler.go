package requesthandler

import (
	"fmt"
	"net/http"
	genericrequesthandler "gitlab.com/kappelmans/go-lang-backend/requesthandler"
	"github.com/gin-gonic/gin"
	"gitlab.com/kappelmans/models/user/controller"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func getCollection(db *mgo.Database, collectionName string) *mgo.Collection {
	return db.C(collectionName)
}

func GetListHandler(db *mgo.Database, c *gin.Context) {
	//validate preconditions

	//launch the querry
	c.JSON(http.StatusOK, controller.GetUserList(getCollection(db, "user")))

	//validate postcondtions

}

func DeleteHandler(db *mgo.Database, c *gin.Context) {
	userKey := c.Param("key")
	//err :=
	getCollection(db, "user").RemoveId(bson.ObjectIdHex(userKey))
	c.JSON(http.StatusOK, "Remove OK: ")
}

func PostHandler(db *mgo.Database, c *gin.Context) {

	col := getCollection(db, "user")

	//Retrieve body from http request
	b, err := c.GetRawData()
	fmt.Println("hello")
	if err != nil {
		panic(err)
	}

	jsonString, _ := controller.CreateUser(col, b)

	c.JSON(http.StatusOK, jsonString)
}

func GetRoutes(basePath string) []genericrequesthandler.Route {

	return []genericrequesthandler.Route{
		genericrequesthandler.Route{
			Action:        genericrequesthandler.GET,
			Path:          basePath + "/list",
			ReqestHandler: GetListHandler,
		},
/*		genericrequesthandler.Route{
			Action:        genericrequesthandler.GET,
			Path:          basePath + "/detail/:key",
			ReqestHandler: GetPartyDetailHandler,
		},*/
		genericrequesthandler.Route{
			Action:        genericrequesthandler.POST,
			Path:          basePath + "/create",
			ReqestHandler: PostHandler,
		},
		genericrequesthandler.Route{
			Action:        genericrequesthandler.DELETE,
			Path:          basePath + "/delete/:key",
			ReqestHandler: DeleteHandler,
		},
	}

}

